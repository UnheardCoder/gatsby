import React from "react"

const BlogLink = () => {
  return (
    <div>
      <p>
        I used my WP blog as a backend for this &rarr;&nbsp;
        <a href={props.link}>Learn More</a>
      </p>
    </div>
  )
}

export default BlogLink
