import React from "react"
import Layout from "../components/layout"
import Img from "gatsby-image"
import { graphql } from "gatsby"

export default ({ data }) => {
  const post = data.allWordpressPost.edges[0].node
  console.log(post.featured_media)
  const resolutions =
    post.featured_media &&
    post.featured_media.localFile.childImageSharp.resolutions
  return (
    <Layout>
      <div>
        <Img className="featured-blog-image" resolutions={resolutions} />
        <h1 className="title-post">{post.title}</h1>
        <div dangerouslySetInnerHTML={{ __html: post.content }} />
      </div>
    </Layout>
  )
}
export const query = graphql`
  query($slug: String!) {
    allWordpressPost(filter: { slug: { eq: $slug } }) {
      edges {
        node {
          title
          content
          featured_media {
            localFile {
              childImageSharp {
                resolutions(width: 600, height: 300) {
                  src
                  width
                  height
                }
              }
            }
          }
        }
      }
    }
  }
`
